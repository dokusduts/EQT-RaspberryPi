# EQT in Raspberry Pi 2

Installatie van de EQT wallet is gedaan op basis van deze post: [https://steemit.com/eqt/@virtualcrypto/raspberry-pi-staking-for-equitrader-eqt](https://steemit.com/eqt/@virtualcrypto/raspberry-pi-staking-for-equitrader-eqt). De installatie werkt niet op mijn Pi.
Nu bleek dat de libdb4.8++-dev niet geïnstalleerd kon worden. Dit kan wel handmatig. Veel stappen zijn identiek aan het originele document. Met onderstaande handeling en volgorde is de installatie op mijn Pi2 gelukt.
 

Commando's uitgevoerd op de pi zijn met gebruiker pi en een $ als prompt
Om commando's uit te voeren met meer rechten (voor installatie) gebruik je sudo voor 
je commando.

Mijn versie van de kernel en de Pi versie:
 
	$ uname -a   

Linux raspberrypi 4.1.19-v7+ #858 SMP Tue Mar 15 15:56:00 GMT 2016 armv7l GNU/Linux
 
	$ cat /etc/os-release 
 
PRETTY_NAME="Raspbian GNU/Linux 7 (wheezy)"  
NAME="Raspbian GNU/Linux"  
VERSION_ID="7"  
VERSION="7 (wheezy)"  
ID=raspbian  
ID_LIKE=debian  
ANSI_COLOR="1;31"  
HOME_URL="http://www.raspbian.org/"  
SUPPORT_URL="http://www.raspbian.org/RaspbianForums"  
BUG_REPORT_URL="http://www.raspbian.org/RaspbianBugs"
 
 
## Update je Pi
 
	$ cd ~   
	$ sudo apt-get update   
	$ sudo apt-get upgrade   
	$ sudo apt-get install -f build-essential autoconf automake git g++ libtool make unzip wget 
 
(Waarschijnlijk zijn een aantal onderdelen al geïnstalleerd)
 
## Install de QR software
 
	$ cd ~   
	$ wget https://fukuchi.org/works/qrencode/qrencode-3.4.4.tar.gz  
	$ tar -zxvf qrencode-3.4.4.tar.gz  
	$ cd qrencode-3.4.4  
	$ ./configure  
	$ make
	$ sudo make install
 
 
## Haal de Equitrade reposetory van Github
 
	$ cd ~  
	$ git clone https://github.com/equitrader/Equitrade.git
 
## Installatie commando uit ~/Equitrade/doc/readme-qt.rst
 
	$ cd ~/Equitrader  
	$ sudo apt-get install qt4-qmake libqt4-dev build-essential libboost-dev libboost-system-dev libboost-filesystem-dev libboost-program-options-dev libboost-thread-dev libssl-dev libdb4.8++-dev
 
Nu krijg je de fout dat de libdb4.8++-dev niet in de repo zit. Je zal nu handmatig de Berkeley-db moeten installeren. 

### Handmatige installatie van berkeley-db 
 
	$ cd ~
	$ wget http://download.oracle.com/berkeley-db/db-4.8.30.NC.tar.gz
	$ sudo tar -xzvf db-4.8.30.NC.tar.gz
	$ cd db-4.8.30.NC/build_unix
	$ sudo ../dist/configure --enable-cxx
	$ sudo make
	$ sudo make install
	$ export CPATH="/usr/local/BerkeleyDB.4.8/include"
	$ export LIBRARY_PATH="/usr/local/BerkeleyDB.4.8/lib"
	$ sudo ln -s /usr/local/BerkeleyDB.4.8/lib/libdb-4.8.so /usr/lib/libdb-4.8.so
	$ sudo ln -s /usr/local/BerkeleyDB.4.8/lib/libdb_cxx-4.8.so /usr/lib/libdb_cxx-4.8.so
 
## Installatie Equitrader

Het installatie commando komt uit de ~/Equitrade/doc/readme-qt.rst file. 
 
	$ cd ~  
	$ sudo apt-get install qt4-qmake libqt4-dev build-essential libboost-dev libboost-system-dev libboost-filesystem-dev libboost-program-options-dev libboost-thread-dev libssl-dev
 
**LETOP:** Dit is dus nu zonder de:  *libdb4.8++-dev* 
 
	$ cd ~/Equitrade  
	$ qmake
	$ make
 
## Starten van de EQT Wallet
 
	$ ~/Equitrade/EquiTrader-qt

En eventueel een snelkoppeleling op de desktop.

	$ ln -s ~/Equitrade/EquiTrader-qt ~/Desktop/EquiTrader-qt-2
	


---



 

 
 
 
 
 