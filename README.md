# Handleiding installatie EQT Wallet op Raspberry Pi2

In deze handleiding een uitleg van de installatie van de EQT Wallet software op een RaspberryPi 2  

Link: [Handleiding NL](https://gitlab.com/dokusduts/EQT-RaspberryPi/blob/master/handleiding_NL.md)

***
***

# Manual for the EQT wallet on a Raspberry P2

In this manual you find an guide to the installation of the EQT Wallet on a RaspberryPi 2

Link: [Manual EN](https://gitlab.com/dokusduts/EQT-RaspberryPi/blob/master/manual_en.md) 