# EQT in Raspberry P2 / P3


This guide is based on this manual: [https://steemit.com/eqt/@virtualcrypto/raspberry-pi-staking-for-equitrader-eqt](https://steemit.com/eqt/@virtualcrypto/raspberry-pi-staking-for-equitrader-eqt). Please read this manual!

_*update:*_ Please be aware that this manual does not work on the latest version Raspbian image. You have to downgrade your Pi.

Tested version: 

|Distributor ID | Description | Release | Codename | Tested | Worked |
|------------|------------|------------|------------|------------|------------|
| Raspbian | Raspbian GNU/Linux 9.1 (stretch)| 9.1 | stretch| yes | **NO** |
| Debian  | Debian GNU/Linux 7.11 (wheezy)| 7.11| wheezy| yes | **YES**|


This guid is only for a wheezy installation ( I will test some more and will update the table above)

Installation failed on `libdb4.8++-dev` so a manually installed of the Berkeley-db was required. A lot of steps<!----> are identical to the original document. With the additional steps, I was able to install the wallet

Commands are executed as pi user starting with the prompt $  For commands that need elevated right use the sudo command. 

This is my version and kernel info of my Pi:
 
	uname -a   

> Linux raspberrypi 4.1.19-v7+ #858 SMP Tue Mar 15 15:56:00 GMT 2016 armv7l GNU/Linux
 
	cat /etc/os-release 
 
> PRETTY_NAME="Raspbian GNU/Linux 7 (wheezy)"  
NAME="Raspbian GNU/Linux"  
VERSION_ID="7"  
VERSION="7 (wheezy)"  
ID=raspbian  
ID_LIKE=debian  	
ANSI_COLOR="1;31"  
HOME_URL="http://www.raspbian.org/"  
SUPPORT_URL="http://www.raspbian.org/RaspbianForums"  
BUG_REPORT_URL="http://www.raspbian.org/RaspbianBugs"
 
 
## Update your Pi
 
	cd ~   
	sudo apt-get update   
	sudo apt-get upgrade   
	sudo apt-get install -f build-essential autoconf automake git g++ libtool make unzip wget 
	 
(Some tools may be installed already)
 
## Install the QR software
 
	cd ~   
	wget https://fukuchi.org/works/qrencode/qrencode-3.4.4.tar.gz  
	tar -zxvf qrencode-3.4.4.tar.gz  
	cd qrencode-3.4.4  
	./configure  
	make
	sudo make install
 
 
## Get your installation files

Download a copy of the Equitrade instalation from Github
 
	cd ~  
	git clone https://github.com/equitrader/Equitrade.git
 
## Install prerequisites

This command is from the  ~/Equitrade/doc/readme-qt.rst file.
 
	cd ~/Equitrader  
	sudo apt-get install qt4-qmake libqt4-dev build-essential libboost-dev libboost-system-dev libboost-filesystem-dev libboost-program-options-dev libboost-thread-dev libssl-dev libdb4.8++-dev
 
This command fails with an error. libdb4.8++-dev is unable to install. below is the manual installation of Berkeley-db. 

### Manual installation of berkeley-db 4.8.30
 
	cd ~
	wget http://download.oracle.com/berkeley-db/db-4.8.30.NC.tar.gz
	sudo tar -xzvf db-4.8.30.NC.tar.gz
	cd db-4.8.30.NC/build_unix
	sudo ../dist/configure --enable-cxx
	sudo make
	sudo make install
	export CPATH="/usr/local/BerkeleyDB.4.8/include"
	export LIBRARY_PATH="/usr/local/BerkeleyDB.4.8/lib"
	sudo ln -s /usr/local/BerkeleyDB.4.8/lib/libdb-4.8.so /usr/lib/libdb-4.8.so
	sudo ln -s /usr/local/BerkeleyDB.4.8/lib/libdb_cxx-4.8.so /usr/lib/libdb_cxx-4.8.so
	 
## Install Equitrader

This command is from the ~/Equitrade/doc/readme-qt.rst file.
 
	cd ~  
	sudo apt-get install qt4-qmake libqt4-dev build-essential libboost-dev libboost-system-dev libboost-filesystem-dev libboost-program-options-dev libboost-thread-dev libssl-dev
 
**ATTENTION:** This is without the option:  *libdb4.8++-dev*
 
	cd ~/Equitrade  
	sudo qmake
	sudo make
 
## Start your EQT Wallet  

To start you EquiTrader wallet:

	~/Equitrade/EquiTrader-qt

To create a shortcut on de Desktop of your Pi user.  

    ln -s ~/Equitrade/EquiTrader-qt ~/Desktop/EquiTrader-qt-2
	


Feel free to open an [issue](https://gitlab.com/dokusduts/EQT-RaspberryPi/issues/new) to help improove the installation prosses.

---